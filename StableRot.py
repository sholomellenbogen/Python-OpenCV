import re
import cv2
import numpy as np
from datetime import datetime


def get_video_frame_size(path_input_vid):
    """ Returns the frame dimensions of input video """
    video = cv2.VideoCapture(path_input_vid)
    # frameCount = 0
    # size = 0
    # while video.isOpened() and frameCount < 1:
    ret, frame = video.read()
    height, width, layers = frame.shape
    size = (width, height)
    # frameCount += 1

    video.release()
    return size


def get_fps(path_input_vid):
    """ Returns the frame rate of input video """

    video = cv2.VideoCapture(path_input_vid)

    # Get fps for video
    fps = int(video.get(cv2.CAP_PROP_FPS))
    video.release()

    # if fps is not between 5-60 then something went wrong so return 30 since that is the most common
    if fps < 5 or fps > 60:
        return 30
    else:
        return fps


def process_ar(path_input_vid, path_template, path_overlay_image, desired_overlay_size, buffer_size, size, fps, path_out_vid, num_frames=0):
    """ Analyses each frame of input video, applies overlay image if AR-tag is found then saves each frame as jpg """

    # number of matching points required to determine if AR-tag was found in frame
    MIN_MATCH_COUNT = 8

    # set up output video variable
    out = cv2.VideoWriter(path_out_vid, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)

    # read in the template image (referred to as 'AR-tag' in earlier comments)
    template = cv2.imread(path_template, 0)

    x_mean = 0
    y_mean = 0
    y_min = 0
    y_max = 0
    x_min = 0
    x_max = 0

    frameCount = 0
    rot_deg = 0

    # read in image which will be overlay on top of the tag in the frames
    inputOverlayPrime = cv2.imread(path_overlay_image)

    # determine the scale factor to resize overlay image to appropriate size
    rows, cols, ch = inputOverlayPrime.shape
    desired_rows, desired_cols = desired_overlay_size
    scale_x = desired_rows/rows
    scale_y = desired_cols/cols
    scalar_x = desired_rows/2
    scalar_y = desired_cols/2

    # Starts to capture
    cap = cv2.VideoCapture(path_input_vid)

    # if num_frames wasn't specified then run for as many frames as are present in the video
    if num_frames == 0:
        num_frames = (cap.get(cv2.CAP_PROP_FRAME_COUNT))

    good = []

    # loop process code while there are frames left to analyse
    while cap.isOpened() and frameCount < num_frames:
        ret, frame = cap.read()

        # get frame in RGB (this is what we will apply the overlay image to and save for later reconstruction)
        img1_RGB = frame

        # gets frame in greyscale (used for detecting the AR-tag)
        img1_Large = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        frameCount += 1

        # so that if it fails, I can easily see which frame caused the failure
        print(frameCount)

        # rotate overlay image
        rotMat = cv2.getRotationMatrix2D((cols / 2, rows / 2), rot_deg, 1)
        inputOverlay_rot = cv2.warpAffine(inputOverlayPrime, rotMat, (cols, rows))

        rot_deg += 3

        # apply scaling factor to overlay image to change size
        img2 = cv2.resize(inputOverlay_rot, None, fx=scale_x, fy=scale_y, interpolation=cv2.INTER_CUBIC)

        # Uses mask to eliminate white background in overlay image
        img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        ret2, mask2 = cv2.threshold(img2gray, 220, 255, cv2.THRESH_BINARY_INV)
        img2_fg = cv2.bitwise_and(img2, img2, mask=mask2)

        # saves inverse of mask, will be used to create black silhouette in frame image  where overlay image will appear
        mask_inv = cv2.bitwise_not(mask2)

        # only analyse every other frame
        if frameCount % 3 == 0:
            """ start of code that analyses the frame to determine if AR-tag is present """

            # reduces the size of frame to 1/4th for faster analysis
            img1 = cv2.pyrDown(cv2.pyrDown(img1_Large))

            # Initiate SIFT detector
            sift = cv2.xfeatures2d.SIFT_create()

            # find the keypoints and descriptors with SIFT
            kp1, des1 = sift.detectAndCompute(img1, None)
            kp2, des2 = sift.detectAndCompute(template, None)

            FLANN_INDEX_KDTREE = 0
            index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
            search_params = dict(checks=50)

            flann = cv2.FlannBasedMatcher(index_params, search_params)

            matches = flann.knnMatch(des1, des2, k=2)

            # store all the good matches as per Lowe's ratio test.
            good = []

            for m, n in matches:
                if m.distance < 0.7 * n.distance:
                    good.append(m)
            """^^^end of code that analyses the frame to determine if AR-tag is present^^^"""

        # Only apply overlay image to frame if more than a set number of matches are found in frame
        if len(good) > MIN_MATCH_COUNT:

            # only analyse every other frame
            if frameCount % 3 == 0:
                # coordinates in frame where a matching point was found for AR-tag
                src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)

                """ Start code to find location of template in frame """
                temp = np.asarray(["0"]).tolist()
                ctr = 0

                for t in np.asarray(src_pts):
                    temp += re.findall(r"[-+]?\d*\.\d+|\d+", str(t))
                even = 0.0
                odd = 0.0
                tempFloat = [float(s) for s in temp]

                while ctr < len(tempFloat) - 15:
                    if ctr % 2 == 0:
                        even += tempFloat[ctr]
                    else:
                        odd += tempFloat[ctr]
                    ctr += 1

                # multiplied by 4 because frame was reduced to 1/4th for analysis
                y_mean_new = 4 * (int(2 * even / (ctr + 0.0001)))
                x_mean_new = 4 * (int(2 * odd / (ctr + 0.0001)))
                if frameCount == 0:
                    y_mean = y_mean_new
                    x_mean = x_mean_new

                if abs(x_mean_new - x_mean) > buffer_size:
                    x_mean = x_mean_new

                if abs(y_mean_new - y_mean) > buffer_size:
                    y_mean = y_mean_new

                y_min = int(y_mean - scalar_y)
                y_max = int(y_mean + scalar_y)
                x_min = int(x_mean - scalar_x)
                x_max = int(x_mean + scalar_x)

                """^^^End code to find location of template in frame^^^"""

            # get dimensions of frame image
            h, w = img1_Large.shape

            # only apply overlay if AR-tag located within bounds of frame image (with 100 unit buffer zone at borders)
            if y_min > 0 and y_max < (h - 100) and x_min > 0 and x_max < (w - 100):

                # Create roi
                roi = img1_RGB[y_min:y_max, x_min:x_max]

                # Now black-out the area in frame where overlay will be placed
                img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)

                # Apply overlay to frame image in location that AR-tag was detected
                dst2 = cv2.add(img1_bg, img2_fg)
                img1_RGB[y_min:y_max, x_min:x_max] = dst2

        # Add frame to output video file
        out.write(img1_RGB)

    out.release()
    cap.release()


def main():
    start_time = datetime.now()

    path_input_vid = 'stable_test_short.mp4'      # path to input video file

    path_template = 'template.png'      # path to template image (AR-tag)

    path_overlay_image = 'rubber_ducky.png'     # path to overlay image

    desired_overlay_size = (320, 320)           # desired size of overlay image

    buffer_size = 80        # number of units (x OR y) AR-tag can move in frame before position of overlay image changes

    size = get_video_frame_size(path_input_vid)     # get the dimensions of frames in input video

    fps = get_fps(path_input_vid)       # get fps of input video

    path_out_vid = 'stable_test_short_OUT_ROT.mp4'       # path of ouput video file

    # process the video
    process_ar(path_input_vid, path_template, path_overlay_image, desired_overlay_size, buffer_size, size, fps, path_out_vid)

    print("runtime = " + str(datetime.now() - start_time))


if __name__ == "__main__":
    main()

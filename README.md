# Python-OpenCV

AUGMENTED REALITY VIDEO LECTURE AID

	Author: Sholom Ellenbogen
	Last Revision Date: August 1, 2018

-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------



SETUP PYTHON INSTRUCTIONS:

	1. install Python 3.5.4 (with pip)
	2. add both python and pip to your path (look this up if unsure how)
	3. type the following into command prompt (cmd) and press enter
	
		pip install numpy matplotlib opencv-contrib-python

	4. Wait for installs to finish


-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------



PROCESSING VIDEO FILES INSTRUCTIONS:

	1. take the following files and place them in a directory (StableReg.py, StableRot.py, template.png, rubber_ducky.png)
	2. place your input video in the same directory
	3. in StableReg.py or StableRot.py edit the properties in main to desired values
	4. Open command prompt (cmd) and change directory until you are in the directory where StableReg.py is
	7. type the following into cmd and press enter

		python StableReg.py

	8. you should see numbers appear on newlines which will scroll down (each number represents a frame in the video)
	9. When the video is done processing you will be shown the runtime.
	10. Now in the same directory you should find the output video


	**NOTE: this program is made so that you can use the keyboard interrupt (CTR+C in cmd) to terminate the program
		early and you will still have an output video that contains all the frames that were processes up to that point.


-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------



